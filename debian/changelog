trocla (0.3.0-0.2) unstable; urgency=medium

  * Non-maintainer upload to unstable

 -- Antoine Beaupré <anarcat@debian.org>  Thu, 31 Mar 2022 15:09:49 -0400

trocla (0.3.0-0.1) experimental; urgency=medium

  [ Antoine Beaupré ]
  * Non-maintainer upload.
  * New upstream release (Closes: #974697)
  * Remove no-pending_for patch: dependency packaged in Debian and added
    to Build-Depends.
  * remove fix_version.patch: just ship the version instead of chasing the
    version number in a patch (!?)
  * disable the failing test, after discussion with upstream (Closes:
    #982154)
  * change section to admin (Closes: #832261)
  * expand search path for sample config file to fix autopkgtest (Closes:
    #830240)

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Use https:// in Vcs-* fields
  * Run wrap-and-sort on packaging files
  * Use new default gem2deb Rakefile to run the tests

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Use secure URI in debian/watch.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

 -- Antoine Beaupré <anarcat@debian.org>  Tue, 04 May 2021 15:32:32 -0400

trocla (0.2.3-1) unstable; urgency=medium

  * Team upload.
  * Imported Upstream version 0.2.3
  * Patch out usage of rspec-pending_for, as we don't have that package yet.

 -- Christian Hofstaedtler <zeha@debian.org>  Tue, 01 Mar 2016 21:41:20 +0100

trocla (0.1.2-1) unstable; urgency=low

  * Initial release (Closes: #777761)

 -- Jonas Genannt <genannt@debian.org>  Tue, 09 Jun 2015 19:56:39 +0200
